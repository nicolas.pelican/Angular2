import { Component } from '@angular/core';

@Component({
	moduleId: module.id,
	template: "Oups, page not found"
})
export class PageNotFoundComponent {}