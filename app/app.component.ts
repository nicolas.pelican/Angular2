import { Component } from '@angular/core';
import { Hike } from './hike/hike';
import { HikeService } from './hike/hike.service';

@Component({
	moduleId: module.id,
  	selector: 'my-app',
  	templateUrl: "app.component.html"
  	//template: '`<h1>Hello {{name}}</h1>`,
})

export class AppComponent  { 
	hikes: Hike[];

	//private hikeService;
/*	constructor(private _hikeService: HikeService){
		this.hike = 
		this.hikeService = _hikeService
	}*/

	constructor(private _hikeService: HikeService){
	}

	ngOnInit() {
		this._hikeService.getHikesFromAPI()
			.subscribe(
				res => this.hikes = res,
				err => console.error(err.status)
			);
		console.log(this.hikes);
	}
	//name = 'Nicolas'; 
}